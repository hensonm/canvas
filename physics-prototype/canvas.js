(function () {
	"use strict";
	var HEIGHT = 480;
	var WIDTH = 640;

	var c = document.createElement("canvas");
	c.height = HEIGHT;
	c.width = WIDTH;
	c.style.border = "1px solid black";
	document.querySelector("body").appendChild(c);

	var ctx = c.getContext("2d");
	var start = Date.now();
	var boxes = [];

//	for (var i = 0; i < 20000; i++) {
//		boxes.push(new Box(ctx, c, Math.random()*30, Math.random()*24, Math.random(), Math.random()));
//	}

	boxes.push($.box(ctx, c, 1, 1, 0.02, 0.98));

	function mainLoop(time) {
		// I have lots of transforms right now
		ctx.save();
		ctx.setTransform(1, 0, 0, 1, 0, 0);
		// Will always clear the right space
		ctx.clearRect(0, 0, WIDTH, HEIGHT);
		ctx.restore();
		// Still have my old transforms

		boxes.forEach(function (it){
			it.move(time - start);

//			if (it.stopped) {
//				boxes.push(new Box(ctx, c, 1, 1, 0.02, 0.98));
//			}
		});

		window.webkitRequestAnimationFrame(mainLoop, c);
		start = Date.now();
	}

	window.webkitRequestAnimationFrame(mainLoop, c);
}());