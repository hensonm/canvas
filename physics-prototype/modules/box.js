// This isn't used as part of the sprite code.
// This is used with the physics stuff
(function ($, undefined) {
	"use strict";

	function Box(context, canvas, x, y, acceleration, drag) {
		this.x = x || 1;
		this.y = y || 1;

		this.verticalVelocity = 0;
		this.horizontalVelocity = 0.2;
		this.acceleration = acceleration !== undefined ? acceleration : .003;
		this.drag = drag || 0.98;

		this.height = 100;
		this.width = 100;

		this.color = "rgb(" + Math.round(Math.random() * 255) + "," + Math.round(Math.random() * 255) + "," + Math.round(Math.random() * 255) + ")";

		this._context = context;
		this._canvas = canvas;
	}

	Box.prototype = {
		get bottom() {
			return this.y + this.height;
		},
		get right() {
			return this.x + this.width;
		},
		get stopped() {
			return !(this.verticalVelocity || this.horizontalVelocity);
		}
	};

	Box.prototype.move = function (timeDelta) {
		if (this.verticalVelocity === 0 && this.horizontalVelocity === 0) {
			this.draw();
			return;
		}

		this.y = Math.round(this.y + (timeDelta * this.verticalVelocity));
		this.x = Math.round(this.x + (timeDelta * this.horizontalVelocity));

		if (this.bottom >= this._canvas.height) {
			this.verticalVelocity *= -1;
		}

		this.verticalVelocity += (timeDelta * this.acceleration);

		if (this.bottom >= HEIGHT) {
			this.horizontalVelocity *= this.drag;
			this.y = HEIGHT - this.height;

			if (Math.abs(this.verticalVelocity) < 2 * (timeDelta * this.acceleration)) {
				this.verticalVelocity = 0;
			}
		}

		if (Math.abs(this.horizontalVelocity) < .01) {
			this.horizontalVelocity = 0;
		} else if (this.right >= WIDTH || this.x <= 0) {
			this.horizontalVelocity *= -1;
		}

		this.draw();
	};

	Box.prototype.draw = function () {
		this._context.fillStyle = this.color;
		this._context.fillRect(this.x, this.y, this.width, this.height);
	};

	$.expand({
		box : function (context, canvas, x, y, acceleration, drag) {
			return new Box(context, canvas, x, y, acceleration, drag);
		}
	});
}($));