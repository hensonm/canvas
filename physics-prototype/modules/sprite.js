//rect.js


(function ($, undefined) {
	"use strict";

	function Sprite (playerControlled, startingX, startingY) {
		this._frame = $.rect(startingX, startingY, 25, 25);
		this._color = "rgb(255, 0, 0)";
		this._playerControlled = !!playerControlled;
	}

	Sprite.prototype = {
		get frame() {
			return this._frame;
		},
		draw : function (ctx, collides) {
			ctx.fillStyle = collides ? "rgb(0,0,0)" : this._color;
			ctx.fillRect(this._frame.origin.x, this._frame.origin.y, this._frame.size.width, this._frame.size.height);
		},
		move : function () {
			if (!this._playerControlled) {
				return;
			}

			var v = 1;

			var keycache = window.engine.keycache; //TODO: fix the global access

			this._frame.x += (RIGHT & keycache) ? v : 0;
			this._frame.x -= (LEFT & keycache) ? v : 0;
			this._frame.y += (DOWN & keycache) ? v : 0;
			this._frame.y -= (UP & keycache) ? v : 0;
		},
		collidesWithRect : function (rect) {
			return this._frame.intersects(rect);
		}
	};

	$.expand({
		sprite : function (pc, startX, startY) {
			return new Sprite(pc, startX, startY);
		}
	});
}($));