(function ($, undefined) {
	"use strict";

	function Board(canvas) {
		this._canvas = canvas;
		this._width = canvas.width;
		this._height = canvas.height;
		this._sprites = [];
	}

	Board.prototype = {
		get width() {
			return this._width;
		},
		get height() {
			return this._height;
		},
		update : function () {
			this._sprites.forEach(function (it) {
				it.move();
			});
		},
		draw : function (ctx) {
			var that = this;
			this._sprites.forEach(function (it) {
				it.draw(ctx, that.checkRectForCollision(it));
			});
		},
		addSprite : function (sprite) {
			this._sprites.push(sprite);
		},
		checkRectForCollision : function (theSprite) {
			var sprite, i;
			for (i = 0; i < this._sprites.length; i++) {
				sprite = this._sprites[i];
				if (sprite !== theSprite && theSprite.collidesWithRect(sprite.frame)) {
					return sprite;
				}
			}

			return null;
		}
	};

	$.expand({
		board : function (width, height) {
			return new Board(width, height);
		}
	});
}($));