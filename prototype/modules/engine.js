(function ($, undefined) {
	"use strict";

	/*
	defined in common.js
	 window.LEFT = 1;
	 window.UP = 2;
	 window.RIGHT = 4;
	 window.DOWN = 8;
	 */

	function Engine (board, context) {
		this._keycache = 0;
		this._board = board;
		this._context = context;

		this._running = true; //For debugging
		this._frameCount = 0;

		//Lets us listen for keypresses!
		document.addEventListener("keydown", this, false);
		document.addEventListener("keyup", this, false);
	}

	Engine.prototype = {
		get keycache() {
			return this._keycache;
		},
		get board() {
			return this._board;
		}
	};

	Engine.prototype.mainLoop = function () {
		var that = this;

		this._board.update();

		if (this._running) {
			window.setTimeout(function () {
				that.mainLoop();
			}, 0);
		}
	};

	Engine.prototype.draw = function () {
		var that = this;

		this._context.save();
		this._context.setTransform(1, 0, 0, 1, 0, 0);
		this._context.clearRect(0, 0, this._board.width, this._board.height);
		this._context.restore();

		this._board.draw(this._context);

		window.requestAnimationFrame(function () {
			that.draw();
		});

		this._frameCount++;
	};

	Engine.prototype.updateFPS = function () {
		//TODO: Make a getter for FPS
		var that = this, output = document.querySelector("#text");
		output.innerText = "FPS: " + this._frameCount;
		this._frameCount = 0;

		window.setTimeout(function () {
			that.updateFPS();
		}, 1000);
	};

	Engine.prototype.start = function () {
		this.mainLoop();
		this.draw();
		this.updateFPS();
	};

	Engine.prototype.handleEvent = function (e) {
		switch (e.type) {
			case "keydown":
				this.keyDown(e.keyCode);
				break;
			case "keyup":
				this.keyUp(e.keyCode);
				break;
		}
	};

	Engine.prototype.keyDown = function (code) {
		switch (code) {
			case 37: //left
				this._keycache |= LEFT;
				break;
			case 38: //up
				this._keycache |= UP;
				break;
			case 39: //right
				this._keycache |= RIGHT;
				break;
			case 40: //down
				this._keycache |= DOWN;
				break;
		}
	};

	Engine.prototype.keyUp = function (code) {
		switch (code) {
			case 37: //left
				this._keycache &= ~(LEFT);
				break;
			case 38: //up
				this._keycache &= ~(UP);
				break;
			case 39: //right
				this._keycache &= ~(RIGHT);
				break;
			case 40: //down
				this._keycache &= ~(DOWN);
				break;
		}
	};

	$.expand({
		engine : function (board, context) {
			return new Engine(board, context);
		}
	});
}($));