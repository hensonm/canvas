(function ($, undefined) {
	"use strict";
	function Rect (x, y, width, height) {
		this._x = x;
		this._y = y;
		this._width = width;
		this._height = height;
	}

	Rect.prototype = {
		get origin() {
			return {x : this._x, y : this._y};
		},
		get size() {
			return {width : this._width, height : this._height};
		},
		get x() {
			return this._x;
		},
		set x(newX) {
			this._x = newX;
		},
		get y() {
			return this._y;
		},
		set y(newY) {
			this._y = newY;
		},
		get xMax() {
			return this._x + this._width - 1;
		},
		get yMax() {
			return this._y + this._height - 1;
		},
		intersects : function (other) {
			return !(this.x > other.xMax || this.y > other.yMax ||
				this.xMax < other.x || this.yMax < other.y)
		}
	};

	$.expand({
		rect : function (x, y, width, height) {
			return new Rect(x, y, width, height);
		}
	});
}($));