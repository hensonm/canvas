(function () {
	"use strict";

	var c = document.createElement("canvas");
	c.height = 480;
	c.width = 640;
	c.style.border = "1px solid black";
	document.querySelector("#thing").appendChild(c);

	var ctx = c.getContext("2d");

	var board = $.board(c);

	board.addSprite($.sprite(true, 0, 0));

//	for (var count = 0; count < 100; count ++) {
//		board.addSprite($.sprite(false, Math.random() * WIDTH, Math.random() * HEIGHT));
//	}

	board.addSprite($.sprite(false, .5 * board.width,.5 * board.height));

	window.engine = $.engine(board, ctx);
	window.engine.start();
}());