if (!window.$) {
	window.$ = {};
}

var LEFT = 1 << 1;
var UP = 1 << 2;
var RIGHT = 1 << 3;
var DOWN = 1 << 4;

(function (Library, undefined) {
	"use strict";

	Library.expand = function (newFunctions) {
		Object.keys(newFunctions).forEach(function (it) {
			if (Library.hasOwnProperty(it)) {
				throw "Trying to overwrite existing key \"" + it + "\"";
			}

			Library[it] = newFunctions[it];
		});
	};
}($));