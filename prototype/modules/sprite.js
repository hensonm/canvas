//rect.js


(function ($, undefined) {
	"use strict";

	function Sprite (playerControlled, startingX, startingY) {
		this._frame = $.rect(startingX, startingY, 25, 25);
		this._color = "rgb(255, 0, 0)";
		this._playerControlled = !!playerControlled;
	}

	Sprite.prototype = {
		get frame() {
			return this._frame;
		},
		draw : function (ctx, collides) {
			ctx.fillStyle = collides ? "rgb(0,0,0)" : this._color;
			ctx.fillRect(this._frame.origin.x, this._frame.origin.y, this._frame.size.width, this._frame.size.height);
		},
		move : function () {
			if (!this._playerControlled) {
				return;
			}

			var v = 1;

			var keycache = window.engine.keycache; //TODO: fix the global access

			//TODO: Bound the movement of sprites to the screen?

			var isRight = !!(RIGHT & keycache); //Need it to be truthy to XOR properly
			if (isRight != !!(LEFT & keycache)) {
//			this._x = (newX < 0) ? 0 : ((newX > WIDTH - this._width) ? WIDTH - this._width : newX);
				this._frame.x += (isRight ? 1 : -1) * v;
			}

			var isDown = !!(DOWN & keycache);
			if (isDown != !!(UP & keycache)) {
//			this._y = (newY < 0) ? 0 : ((newY > HEIGHT - this._height) ? HEIGHT - this._height : newY);
				this._frame.y += (isDown ? 1 : -1) * v;
			}
		},
		collidesWithRect : function (rect) {
			return this._frame.intersects(rect);
		}
	};

	$.expand({
		sprite : function (pc, startX, startY) {
			return new Sprite(pc, startX, startY);
		}
	});
}($));